import * as express from 'express';
import { MicroframeworkLoader, MicroframeworkSettings } from 'microframework-w3tec';
import * as path from 'path';
import favicon from 'serve-favicon';

export const publicLoader: MicroframeworkLoader = (settings: MicroframeworkSettings | undefined) => {
	if (settings) {
		const expressApp = settings.getData('express_app');
		console.log('SERVING STATIC ASSETS FROM', path.join(__dirname, '../../src', 'public'));
		expressApp
			// Serve static filles like images from the public folder
			.use(express.static(path.join(__dirname, '../../src', 'public'), { setHeaders: (res) => {
				res.setHeader('Cache-Control', 'max-age: 86400');
			}}))

			// A favicon is a visual cue that client software, like browsers, use to identify a site
			.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));

	}
};
