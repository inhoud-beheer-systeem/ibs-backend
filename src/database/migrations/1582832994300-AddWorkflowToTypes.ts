import {MigrationInterface, QueryRunner, TableColumn} from 'typeorm';

export class AddWorkflowToTypes1582832994300 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'workflowUuid',
			type: 'varchar',
			length: '255',
			isPrimary: false,
			isNullable: true,
		});

		queryRunner.addColumn('content_type', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// Do nothing
	}

}
