import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreatePage1577735317004 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const table = new Table({
			name: 'page',
			columns: [
				{
					name: 'uuid',
					type: 'varchar',
					length: '255',
					isPrimary: true,
					isUnique: true,
					isNullable: false,
				}, {
					name: 'pageTypeUuid',
					type: 'varchar',
					length: '255',
					isPrimary: false,
					isNullable: false,
					isUnique: true,
				}, {
					name: 'fields',
					type: 'jsonb',
					isPrimary: false,
					isNullable: false,
				}, {
					name: 'tenantUuid',
					type: 'varchar',
					length: '255',
					isPrimary: false,
					isNullable: false,
				}, {
					name: 'updatedAt',
					type: 'timestamp',
					isPrimary: false,
					isNullable: false,
				}, {
					name: 'createdAt',
					type: 'timestamp',
					isPrimary: false,
					isNullable: false,
				},
			],
		});
		await queryRunner.createTable(table);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.dropTable('page');
	}

}
