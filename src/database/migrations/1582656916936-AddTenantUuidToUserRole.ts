import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddTenantUuidToUserRole1582656916936 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const column = new TableColumn({
			name: 'tenantUuid',
			type: 'varchar',
			length: '255',
			isPrimary: false,
			isNullable: false,
		});

		queryRunner.addColumn('user_role', column);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		// Do nothing
	}

}
