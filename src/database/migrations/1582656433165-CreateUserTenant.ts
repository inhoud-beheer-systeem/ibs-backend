import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateUserTenant1582656433165 implements MigrationInterface {

	public async up(queryRunner: QueryRunner): Promise<any> {
		const table = new Table({
			name: 'user_tenants_tenant',
			columns: [
				{
					name: 'userUuid',
					type: 'varchar',
					length: '255',
					isNullable: false,
				}, {
					name: 'tenantUuid',
					type: 'varchar',
					length: '255',
					isNullable: false,
				},
			],
		});
		await queryRunner.createTable(table);
	}

	public async down(queryRunner: QueryRunner): Promise<any> {
		await queryRunner.dropTable('user_tenants_tenant');
	}

}
