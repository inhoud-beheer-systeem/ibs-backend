import { Factory, Seed } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';
import { Tenant } from '../../../src/api/models/Tenant';

import uuid = require('uuid');

export class CreateTenant implements Seed {

	public async seed(factory: Factory, connection: Connection): Promise<any> {
		await connection
			.createQueryBuilder()
			.insert()
			.into(Tenant)
			.values([{
				uuid: uuid.v4(),
				name: 'Axel Van Thielen',
				slug: 'axelvanthielen',
				url: 'axelvanthielen.be',
				updatedAt: new Date(),
				createdAt: new Date(),
			}])
			.execute();
	}

}
