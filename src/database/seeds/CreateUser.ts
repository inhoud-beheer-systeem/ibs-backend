import { Factory, Seed } from 'typeorm-seeding';
import { Connection } from 'typeorm/connection/Connection';

import { User } from '../../../src/api/models/User';

import uuid = require('uuid');

export class CreateUser implements Seed {

	public async seed(factory: Factory, connection: Connection): Promise<any> {
		await connection
			.createQueryBuilder()
			.insert()
			.into(User)
			.values([{
				uuid: uuid.v4(),
				firstName: 'Felikx',
				lastName: 'Van Saet',
				password: '$2y$12$T4Rcu5Q4rxUEFiC12SvtsesIG7lrDio12sj7oMRh2g7d.bkq/.VFa',
				email: 'hybridfox2@gmail.com',
				updatedAt: new Date(),
				createdAt: new Date(),
			}])
			.execute();
	}

}
