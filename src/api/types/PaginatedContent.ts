export interface PaginatedContent<T> {
	_entities: T;
	_page: {
		totalEntities: number
	};
}
