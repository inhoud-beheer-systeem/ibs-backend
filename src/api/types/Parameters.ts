interface GenericParameters {
	tenant: string;
	contentTypeSlug: string;
	filters?: any;
	skip?: number;
	limit?: number;
	sort?: any;
	showUnpublished: boolean;
}

export interface ContentParameters extends GenericParameters {
	contentTypeSlug: string;
	populate?: boolean;
	language?: string;
}
