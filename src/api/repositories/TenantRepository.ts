import { EntityRepository, Repository } from 'typeorm';

import { Tenant } from '../models/Tenant';

@EntityRepository(Tenant)
export class TenantRepository extends Repository<Tenant> {

}
