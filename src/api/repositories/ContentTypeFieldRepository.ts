import { EntityRepository, Repository } from 'typeorm';

import { ContentTypeField } from '../models/ContentTypeField';

@EntityRepository(ContentTypeField)
export class ContentTypeFieldRepository extends Repository<ContentTypeField> {

}
