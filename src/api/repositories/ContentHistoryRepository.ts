import { EntityRepository, Repository } from 'typeorm';

import { ContentHistory } from '../models/ContentHistory';

@EntityRepository(ContentHistory)
export class ContentHistoryRepository extends Repository<ContentHistory> {

}
