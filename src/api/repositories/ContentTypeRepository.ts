import { EntityRepository, Repository } from 'typeorm';

import { ContentType } from '../models/ContentType';

@EntityRepository(ContentType)
export class ContentTypeRepository extends Repository<ContentType> {

}
