import { EntityRepository, Repository } from 'typeorm';

import { Workflow } from '../models/Workflow';

@EntityRepository(Workflow)
export class WorkflowRepository extends Repository<Workflow> {

}
