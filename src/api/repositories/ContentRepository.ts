import { EntityRepository, Repository } from 'typeorm';

import { Content } from '../models/Content';

@EntityRepository(Content)
export class ContentRepository extends Repository<Content> {

}
