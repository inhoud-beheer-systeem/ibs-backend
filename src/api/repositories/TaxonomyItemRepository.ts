import { EntityRepository, Repository } from 'typeorm';

import { TaxonomyItem } from '../models/TaxonomyItem';

@EntityRepository(TaxonomyItem)
export class TaxonomyItemRepository extends Repository<TaxonomyItem> {

}
