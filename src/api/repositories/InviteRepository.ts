import { EntityRepository, Repository } from 'typeorm';

import { Invite } from '../models/Invite';

@EntityRepository(Invite)
export class InviteRepository extends Repository<Invite>  {

}
