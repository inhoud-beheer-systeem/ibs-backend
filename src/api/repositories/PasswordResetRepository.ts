import { EntityRepository, Repository } from 'typeorm';

import { PasswordReset } from '../models/PasswordReset';

@EntityRepository(PasswordReset)
export class PasswordResetRepository extends Repository<PasswordReset>  {

}
