import { EntityRepository, Repository } from 'typeorm';

import { TenantLanguage } from '../models/TenantLanguage';

@EntityRepository(TenantLanguage)
export class TenantLanguageRepository extends Repository<TenantLanguage> {

}
