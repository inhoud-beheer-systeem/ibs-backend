import { EntityRepository, Repository } from 'typeorm';

import { WorkflowState } from '../models/WorkflowState';

@EntityRepository(WorkflowState)
export class WorkflowStateRepository extends Repository<WorkflowState> {

}
