import { EntityRepository, Repository } from 'typeorm';

import { Taxonomy } from '../models/Taxonomy';

@EntityRepository(Taxonomy)
export class TaxonomyRepository extends Repository<Taxonomy> {

}
