import { EntityRepository, Repository } from 'typeorm';

import { PageTypeField } from '../models/PageTypeField';

@EntityRepository(PageTypeField)
export class PageTypeFieldRepository extends Repository<PageTypeField> {

}
