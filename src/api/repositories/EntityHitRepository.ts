import { EntityRepository, Repository } from 'typeorm';

import { EntityHit } from '../models/EntityHit';

@EntityRepository(EntityHit)
export class EntityHitRepository extends Repository<EntityHit> {

}
