import { EntityRepository, Repository } from 'typeorm';

import { View } from '../models/View';

@EntityRepository(View)
export class ViewRepository extends Repository<View> {

}
