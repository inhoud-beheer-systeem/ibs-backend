import { EntityRepository, Repository } from 'typeorm';

import { RolePermission } from '../models/RolePermission';

@EntityRepository(RolePermission)
export class RolePermissionRepository extends Repository<RolePermission>  {

}
