import { EntityRepository, Repository } from 'typeorm';

import { PageType } from '../models/PageType';

@EntityRepository(PageType)
export class PageTypeRepository extends Repository<PageType> {

}
