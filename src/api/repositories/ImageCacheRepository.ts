import { EntityRepository, Repository } from 'typeorm';

import { ImageCache } from '../models/ImageCache';

@EntityRepository(ImageCache)
export class ImageCacheRepository extends Repository<ImageCache> {

}
