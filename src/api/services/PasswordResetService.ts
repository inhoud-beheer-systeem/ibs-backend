import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import uuid from 'uuid';

import { PasswordReset } from '../models/PasswordReset';
import { PasswordResetRepository } from '../repositories/PasswordResetRepository';
import { User } from '../models/User';
import { MailHelper } from '../helpers/MailHelper';

@Service()
export class PasswordResetService {

	constructor(
		@OrmRepository() private passwordResetRepository: PasswordResetRepository,
		private mailHelper: MailHelper
	) { }

	public findOne(search: any): Promise<PasswordReset | undefined> {
		return this.passwordResetRepository.findOne(search);
	}

	public async create(passwordReset: PasswordReset): Promise<PasswordReset> {
		return await this.passwordResetRepository.save({
			uuid: uuid.v4(),
			...passwordReset,
			createdAt: new Date(),
			updatedAt: new Date(),
		});
	}

	public async sendPasswordResetMail(passwordReset: PasswordReset, user: User): Promise<void> {
		return await this.mailHelper.sendMail({
			to: passwordReset.emailAddress,
			subject: `Reset your password`,
			context: { passwordReset, user },
			template: 'passwordReset',
		});
	}

	public async delete(passwordResetUuid: string): Promise<void> {
		await this.passwordResetRepository.delete(passwordResetUuid);
		return;
	}
}
