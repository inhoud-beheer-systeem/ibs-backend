import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Role } from '../models/Role';
import { RolePermissionRepository } from '../repositories/RolePermissionRepository';
import { RoleRepository } from '../repositories/RoleRepository';
import { PaginatedContent } from '../types/PaginatedContent';

import uuid = require('uuid');

@Service()
export class RoleService {

	constructor(
		@OrmRepository() private roleRepository: RoleRepository,
		@OrmRepository() private rolePermissionRepository: RolePermissionRepository
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<Role[]>> {
		const query = this.roleRepository.createQueryBuilder('role')
			.leftJoinAndSelect('role.permissions', 'permission')
			.where('role.tenantUuid = :tenant', { tenant });

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<Role | undefined> {
		return this.roleRepository.findOne(search);
	}

	public async create(role: any, tenant: string): Promise<Role> {
		role.permissions = Object.keys(role.permissions).reduce((acc, permissionKey) => {
			const permissionEnabled = role.permissions[permissionKey];
			if (permissionEnabled) {
				return [
					...acc,
					{
						uuid: uuid.v4(),
						permission: permissionKey,
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				];
			}

			return acc;
		}, []);

		return this.roleRepository.save({
			...role,
			tenantUuid: tenant,
		});
	}

	public async update(id: string, role: Role): Promise<Role> {
		// First we find the current fields and kill em off
		await this.rolePermissionRepository.delete({
			roleUuid: id,
		});

		role.permissions = Object.keys(role.permissions).reduce((acc, permissionKey) => {
			const permissionEnabled = role.permissions[permissionKey];
			if (permissionEnabled) {
				return [
					...acc,
					{
						uuid: uuid.v4(),
						permission: permissionKey,
						createdAt: new Date(),
						updatedAt: new Date(),
					},
				];
			}

			return acc;
		}, []);
		role.uuid = id;
		return this.roleRepository.save(role);
	}

	public async delete(id: string): Promise<void> {
		await this.roleRepository.delete(id);
		return;
	}

}
