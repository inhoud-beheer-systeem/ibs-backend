import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { ContentType } from '../models/ContentType';
import { ContentTypeRepository } from '../repositories/ContentTypeRepository';
import { ContentTypeFieldRepository } from '../repositories/ContentTypeFieldRepository';
import { PaginatedContent } from '../types/PaginatedContent';

import uuid = require('uuid');
import { ContentTypeField } from '../models/ContentTypeField';
@Service()
export class ContentTypeService {

	constructor(
		@OrmRepository() private contentTypeRepository: ContentTypeRepository,
		@OrmRepository() private contentTypeFieldRepository: ContentTypeFieldRepository
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<ContentType[]>> {
		const query = this.contentTypeRepository.createQueryBuilder('Content')
				.where('Content.tenantUuid = :tenant', { tenant })
				.leftJoinAndSelect('Content.fields', 'Fields')
				.leftJoinAndSelect('Content.workflow', 'Workflow')
				.leftJoinAndSelect('Workflow.states', 'states')
				.leftJoinAndSelect('Fields.subfields', 'Subfields')
				.orderBy('Fields.order', 'ASC');

		return {
			_entities: await query.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<ContentType | undefined> {
		return this.contentTypeRepository.createQueryBuilder('Content')
			.where('Content.uuid = :id', { id })
			.leftJoinAndSelect('Content.fields', 'Fields')
			.leftJoinAndSelect('Content.workflow', 'Workflow')
			.leftJoinAndSelect('Workflow.states', 'states')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC')
			.getOne();
	}

	public async create(tenant: string, contentType: ContentType): Promise<ContentType> {
		contentType.uuid = uuid.v4();
		contentType.createdAt = new Date();
		contentType.updatedAt = new Date();
		contentType.tenantUuid = tenant;
		contentType.workflow = {
			uuid: contentType.workflow,
		} as any;
		contentType.fields = this.mapFieldsWithOrder(contentType.uuid, tenant, contentType.fields || []);

		return await this.contentTypeRepository.save(contentType);
	}

	public async update(id: string, tenant: string, contentType: ContentType): Promise<any> {
		// First we find the current fields and kill em off
		await this.contentTypeFieldRepository.delete({
			contentTypeUuid: id,
		});

		contentType.uuid = id;
		contentType.updatedAt = new Date();
		contentType.workflow = {
			uuid: contentType.workflow,
		} as any;
		contentType.fields = this.mapFieldsWithOrder(contentType.uuid, tenant, contentType.fields || []);

		return this.contentTypeRepository.save(contentType);
	}

	public async delete(id: string): Promise<void> {
		await this.contentTypeRepository.delete(id);
		return;
	}

	private mapFieldsWithOrder(contentTypeUuid: string, tenant: string, fields: ContentTypeField[], isSubfield: boolean = false): ContentTypeField[] {
		let order = 0;
		return fields.map((field) => ({
			contentTypeUuid: isSubfield ? undefined : contentTypeUuid,
			createdAt: new Date(),
			...field,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			updatedAt: new Date(),
			order: order++,
			subfields: this.mapFieldsWithOrder(contentTypeUuid, tenant, field.subfields || [], true),
		}));
	}
}
