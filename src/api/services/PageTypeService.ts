import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { PageType } from '../models/PageType';
import { PageRepository } from '../repositories/PageRepository';
import { PageTypeRepository } from '../repositories/PageTypeRepository';
import { PaginatedContent } from '../types/PaginatedContent';
import { PageTypeFieldRepository } from '../repositories/PageTypeFieldRepository';

import uuid = require('uuid');
import { PageTypeField } from '../models/PageTypeField';
@Service()
export class PageTypeService {

	constructor(
		@OrmRepository() private pageTypeRepository: PageTypeRepository,
		@OrmRepository() private pageRepository: PageRepository,
		@OrmRepository() private pageTypeFieldRepository: PageTypeFieldRepository
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<PageType[]>> {
		const query = this.pageTypeRepository.createQueryBuilder('c')
			.where('c.tenantUuid = :tenant', { tenant })
			.leftJoinAndSelect('c.fields', 'Fields')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC');

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<PageType | undefined> {
		return this.pageTypeRepository.createQueryBuilder('c')
			.where('c.uuid = :id', { id })
			.leftJoinAndSelect('c.fields', 'Fields')
			.leftJoinAndSelect('Fields.subfields', 'Subfields')
			.orderBy('Fields.order', 'ASC')
			.getOne();
	}

	public async create(tenant: string, pageType: PageType): Promise<PageType> {
		pageType.uuid = uuid.v4();
		pageType.createdAt = new Date();
		pageType.updatedAt = new Date();
		pageType.tenantUuid = tenant;
		pageType.fields = this.mapFieldsWithOrder(pageType.uuid, tenant, pageType.fields || []);

		await this.pageRepository.save({
			uuid: uuid.v4(),
			pageType,
			tenantUuid: tenant,
			fields: {},
			createdAt: new Date(),
			updatedAt: new Date(),
		});

		return await this.pageTypeRepository.save(pageType);
	}

	public async update(id: string, tenant: string, pageType: PageType): Promise<any> {
		await this.pageTypeFieldRepository.delete({
			pageTypeUuid: id,
		});

		pageType.uuid = id;
		pageType.updatedAt = new Date();
		pageType.fields = this.mapFieldsWithOrder(pageType.uuid, tenant, pageType.fields || []);
		console.log(pageType.fields);

		return this.pageTypeRepository.save(pageType);
	}

	public async delete(id: string): Promise<void> {
		await this.pageTypeRepository.delete(id);
		return;
	}

	private mapFieldsWithOrder(pageTypeUuid: string, tenant: string, fields: PageTypeField[], isSubfield: boolean = false): PageTypeField[] {
		let order = 0;
		return fields.map((field) => ({
			pageTypeUuid: isSubfield ? undefined : pageTypeUuid,
			createdAt: new Date(),
			...field,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			updatedAt: new Date(),
			order: order++,
			subfields: this.mapFieldsWithOrder(pageTypeUuid, tenant, field.subfields || [], true),
		}));
	}
}
