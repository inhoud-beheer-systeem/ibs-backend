import { prop, pathOr } from 'ramda';
import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { User } from '../models/User';
import { UserRepository } from '../repositories/UserRepository';
import { UserRoleRepository } from '../repositories/UserRole';
import { PaginatedContent } from '../types/PaginatedContent';

import uuid = require('uuid');
@Service()
export class UserService {

	constructor(
		@OrmRepository() private userRepository: UserRepository,
		@OrmRepository() private userRoleRepository: UserRoleRepository
	) { }

	public async find(skip: number = 0, limit: number = 20): Promise<PaginatedContent<User[]>> {
		const query = this.userRepository.createQueryBuilder();

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<User | undefined> {
		return this.userRepository.findOne(search);
	}

	public async create(user: Omit<User, 'hashPassword'>): Promise<User> {
		user.tenants = user.tenants.map((tenantUuid) => ({ uuid: tenantUuid })) as any;
		const newUser = await this.userRepository.save(user);
		return newUser;
	}

	public update(id: string, user: User): Promise<User> {
		user.uuid = id;
		if (typeof pathOr(undefined, ['tenants', 0])(user) !== 'object') {
			user.tenants = user.tenants.map((tenantUuid) => ({ uuid: tenantUuid })) as any;
		}
		return this.userRepository.save(user);
	}

	public async delete(id: string): Promise<void> {
		await this.userRepository.delete(id);
		return;
	}

	public async assignRole(userUuid: string, tenantUuid: string, roleUuid: string): Promise<void> {
		await this.userRoleRepository.delete({
			tenantUuid,
			userUuid,
		});

		await this.userRoleRepository.save({
			uuid: uuid.v4(),
			tenantUuid,
			roleUuid,
			userUuid,
		});
		return;
	}

	public async getRole(userUuid: string, tenantUuid: string): Promise<any> {
		const roleRelation = await this.userRoleRepository.findOne({
			tenantUuid,
			userUuid,
		});

		return prop('role')(roleRelation);
	}
}
