import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { ImageCache } from '../models/ImageCache';

import { ImageCacheRepository } from '../repositories/ImageCacheRepository';
import uuid = require('uuid');

@Service()
export class ImageCacheService {

	constructor(
		@OrmRepository() private imageCacheRepository: ImageCacheRepository
	) { }

	public findOne(tenantUuid: string, slug: string): Promise<ImageCache | undefined> {
		return this.imageCacheRepository.findOne({
			tenantUuid, slug,
		});
	}

	public async create(tenantUuid: string, imageData: Buffer, slug: string): Promise<ImageCache> {
		const imageCache = new ImageCache();
		imageCache.uuid = uuid.v4();
		imageCache.createdAt = new Date();
		imageCache.updatedAt = new Date();
		imageCache.tenantUuid = tenantUuid;
		imageCache.slug = slug;
		imageCache.data = imageData;

		return await this.imageCacheRepository.save(imageCache);
	}

	public async delete(tenantUuid: string): Promise<void> {
		await this.imageCacheRepository.delete({
			tenantUuid,
		});
		return;
	}

}
