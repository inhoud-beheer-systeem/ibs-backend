import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { env } from '../../env';
import { Tenant } from '../models/Tenant';
import { Invite } from '../models/Invite';
import { InviteRepository } from '../repositories/InviteRepository';
import { PaginatedContent } from '../types/PaginatedContent';
import uuid from 'uuid';
import { User } from '../models/User';
import { MailHelper } from '../helpers/MailHelper';

@Service()
export class InviteService {

	constructor(
		@OrmRepository() private inviteRepository: InviteRepository,
		private mailHelper: MailHelper
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<Invite[]>> {
		const query = this.inviteRepository.createQueryBuilder('Invite')
			.leftJoinAndSelect('Invite.role', 'Role')
			.where('Invite.tenantUuid = :tenant', { tenant });

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<Invite | undefined> {
		return this.inviteRepository.findOne(search);
	}

	public async create(tenant: string, invite: Invite): Promise<Invite> {
		return await this.inviteRepository.save({
			uuid: uuid.v4(),
			...invite,
			tenantUuid: tenant,
			createdAt: new Date(),
			updatedAt: new Date(),
		});
	}

	public async sendRegistrationInviteMail(tenant: Tenant, invite: Invite): Promise<void> {
		return await this.mailHelper.sendMail({
			to: invite.emailAddress,
			subject: `You have been invited to collaborate on ${tenant.name}`,
			context: { tenant, env, invite },
			template: 'registrationInvite',
			tenant: tenant.uuid,
		});
	}

	public async sendTenantInviteMail(tenant: Tenant, invite: Invite, user: User): Promise<void> {
		return await this.mailHelper.sendMail({
			to: invite.emailAddress,
			subject: `You have been invited to collaborate on ${tenant.name}`,
			context: { tenant, env, invite, user },
			template: 'tenantInvite',
			tenant: tenant.uuid,
		});
	}

	public async delete(inviteUuid: string): Promise<void> {
		await this.inviteRepository.delete(inviteUuid);
		return;
	}
}
