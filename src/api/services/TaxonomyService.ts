import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Taxonomy } from '../models/Taxonomy';
import { TaxonomyRepository } from '../repositories/TaxonomyRepository';
import { PaginatedContent } from '../types/PaginatedContent';
import { TaxonomyItemRepository } from '../repositories/TaxonomyItemRepository';

import uuid = require('uuid');
import { TaxonomyItem } from '../models/TaxonomyItem';

@Service()
export class TaxonomyService {

	constructor(
		@OrmRepository() private taxonomyRepository: TaxonomyRepository,
		@OrmRepository() private taxonomyItemRepository: TaxonomyItemRepository
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<Taxonomy[]>> {
		const query = this.taxonomyRepository.createQueryBuilder('t')
			.where('t.tenantUuid = :tenant', { tenant });

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(id: string): Promise<Taxonomy | undefined> {
		return this.taxonomyRepository.findOne(id);
	}

	public async create(tenant: string, taxonomy: Taxonomy): Promise<Taxonomy> {
		taxonomy.uuid = uuid.v4();
		taxonomy.createdAt = new Date();
		taxonomy.updatedAt = new Date();
		taxonomy.tenantUuid = tenant;
		taxonomy.items = (taxonomy.items || []).map((item: TaxonomyItem): TaxonomyItem => ({
			...item,
			uuid: uuid.v4(),
			tenantUuid: tenant,
			createdAt: new Date(),
			updatedAt: new Date(),
		}));

		return await this.taxonomyRepository.save(taxonomy);
	}

	public async update(id: string, tenant: string, taxonomy: Taxonomy): Promise<any> {
		await this.taxonomyItemRepository.delete({
			uuid: id,
		});

		taxonomy.uuid = id;
		taxonomy.updatedAt = new Date();
		taxonomy.items = (taxonomy.items || []).map((state: TaxonomyItem): TaxonomyItem => ({
			createdAt: new Date(),
			uuid: uuid.v4(),
			...state,
			taxonomyUuid: id,
			tenantUuid: tenant,
			updatedAt: new Date(),
		}));
		console.log(taxonomy);

		return this.taxonomyRepository.save(taxonomy);
	}

	public async delete(id: string): Promise<void> {
		await this.taxonomyRepository.delete(id);
		return;
	}

}
