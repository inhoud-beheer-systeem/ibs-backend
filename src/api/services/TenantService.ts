import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Tenant } from '../models/Tenant';
import { TenantRepository } from '../repositories/TenantRepository';
import { PaginatedContent } from '../types/PaginatedContent';

@Service()
export class TenantService {

	constructor(
		@OrmRepository() private tenantRepository: TenantRepository
	) { }

	public async find(skip: number = 0, limit: number = 20): Promise<PaginatedContent<Tenant[]>> {
		const query = this.tenantRepository.createQueryBuilder();

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<Tenant | undefined> {
		return this.tenantRepository.findOne(search, {
			relations: ['users'],
		});
	}

	public async create(tenant: Tenant): Promise<Tenant> {
		const newTenant = await this.tenantRepository.save(tenant);
		return newTenant;
	}

	public update(id: string, tenant: Tenant): Promise<Tenant> {
		tenant.uuid = id;
		return this.tenantRepository.save(tenant);
	}

	public async delete(id: string): Promise<void> {
		await this.tenantRepository.delete(id);
		return;
	}

}
