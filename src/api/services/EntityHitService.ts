import moment from 'moment';
import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { EntityHit } from '../models/EntityHit';
import { EntityHitRepository } from '../repositories/EntityHitRepository';

import uuid = require('uuid');

@Service()
export class EntityHitService {

	constructor(
		@OrmRepository() private entityHitRepository: EntityHitRepository
	) { }

	public async getCountFromDate(startDate: Date, tenant: string): Promise<any> {
		const query = this.entityHitRepository.createQueryBuilder('Hit');

		query
			.where('Hit.createdAt > :startDate', { startDate })
			.andWhere('Hit.tenantUuid = :tenant', { tenant });

		return query.getCount();
	}

	public async getPopularEntities(entityType: 'page' | 'content', tenant: string): Promise<any> {
		const query = this.entityHitRepository.createQueryBuilder('hit');

		query
			.where('hit.createdAt > :startDate', { startDate: moment().subtract(1, 'month') })
			.andWhere('hit.tenantUuid = :tenant', { tenant })
			.andWhere('hit.entityType = :entityType', { entityType })
			.orderBy('count', 'DESC')
			.limit(5)
			.innerJoin(`hit.${entityType}`, 'entity');

		// I am not sorry
		if (entityType === 'content') {
			query
				.groupBy('hit.entityUuid, entity.name')
				.select('entity.name, hit.entityUuid, COUNT(hit.entityUuid) as count');
		} else if (entityType === 'page') {
			query
				.groupBy('hit.entityUuid, entity.pageType, pageType.name')
				.innerJoin('entity.pageType', 'pageType')
				.select('entity.pageType, pageType.name, hit.entityUuid, COUNT(hit.entityUuid) as count');
		}

		return query.getRawMany();
	}

	public async getCountByDay(tenant: string): Promise<any> {
		const query = this.entityHitRepository.createQueryBuilder('hit');

		query
			.select("to_char(hit.createdAt, 'YYYY-MM-DD') as name, COUNT(1) as value")
			.where('hit.createdAt > :startDate', { startDate: moment().subtract(1, 'week') })
			.andWhere('hit.tenantUuid = :tenant', { tenant })
			.groupBy('1');

		const dates = await query.getRawMany();

		return [...Array(6).keys()].reduce((acc, daysAgo) => {
			const formattedDate = moment().subtract(daysAgo, 'day').format('YYYY-MM-DD');
			const existingDate = dates.find((date) => date.name === formattedDate);

			if (existingDate) {
				acc.push(existingDate);
			} else {
				acc.push({
					name: formattedDate,
					value: '0',
				});
			}

			return acc;
		}, [])
			.sort((a, b) => (new Date(b.name) as any) - (new Date(a.name) as any));
	}

	public findOne(search: any): Promise<EntityHit | undefined> {
		return this.entityHitRepository.findOne(search);
	}

	public async create(entityType: string, entityUuid: string, tenantUuid: string): Promise<EntityHit> {
		const newEntityHit = await this.entityHitRepository.save({
			uuid: uuid.v4(),
			entityUuid,
			entityType,
			tenantUuid,
			extraInfo: {},
			createdAt: new Date(),
			updatedAt: new Date(),
		});
		return newEntityHit;
	}
}
