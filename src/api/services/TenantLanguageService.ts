import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { TenantLanguage } from '../models/TenantLanguage';
import { TenantLanguageRepository } from '../repositories/TenantLanguageRepository';
import { PaginatedContent } from '../types/PaginatedContent';

@Service()
export class TenantLanguageService {

	constructor(
		@OrmRepository() private tenantLanguageRepository: TenantLanguageRepository
	) { }

	public async find(tenant: string, skip: number = 0, limit: number = 20): Promise<PaginatedContent<TenantLanguage[]>> {
		const query = this.tenantLanguageRepository.createQueryBuilder('language')
			.where('language.tenantUuid = :tenant', { tenant });

		return {
			_entities: await query
				.offset(skip)
				.limit(limit)
				.getMany(),
			_page: {
				totalEntities: await query.getCount(),
			},
		};
	}

	public findOne(search: any): Promise<TenantLanguage | undefined> {
		return this.tenantLanguageRepository.findOne(search);
	}

	public async create(tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		const newTenantLanguage = await this.tenantLanguageRepository.save(tenantLanguage);
		return newTenantLanguage;
	}

	public update(id: string, tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		tenantLanguage.uuid = id;
		return this.tenantLanguageRepository.save(tenantLanguage);
	}

	public async delete(id: string): Promise<void> {
		await this.tenantLanguageRepository.delete(id);
		return;
	}
}
