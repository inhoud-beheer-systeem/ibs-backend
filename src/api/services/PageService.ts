import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';

import { Page } from '../models/Page';
import { ContentRepository } from '../repositories/ContentRepository';
import { PageRepository } from '../repositories/PageRepository';
import { PageTypeRepository } from '../repositories/PageTypeRepository';

@Service()
export class PageService {

	constructor(
		@OrmRepository() private pageRepository: PageRepository,
		@OrmRepository() private pageTypeRepository: PageTypeRepository,
		@OrmRepository() private contentRepository: ContentRepository
	) { }

	public async findOne(pageTypeId: string, options: any = {}): Promise<Page | undefined> {
		const pageType = await this.pageTypeRepository.findOne({
			slug: pageTypeId,
		});

		const page = await this.pageRepository.findOne({
			pageTypeUuid: pageType.uuid,
		});

		if (options.populate) {
			// Check out for which fields we need to populate
			const toPopulateFields = page.pageType.fields.filter((field) => field.fieldType === 'content-input');
			const toPopulateValues = await toPopulateFields.reduce(async (acc, field) => {
				if (!Array.isArray(page.fields[field.slug])) {
					return { ...acc, [field.slug]: await this.contentRepository.findOne(page.fields[field.slug]) };
				}

				const promises = Promise.all(page.fields[field.slug].map((subUuid) => this.contentRepository.findOne(subUuid)));
				return { ...acc, [field.slug]: await promises };
			}, {});

			return {
				...page,
				fields: {
					...page.fields,
					...toPopulateValues,
				},
			};
		}

		return page;
	}

	public async update(pageTypeId: string, updatedPage: Page): Promise<any> {
		const pageType = await this.pageTypeRepository.findOne({
			slug: pageTypeId,
		});

		const page = await this.pageRepository.findOne({
			pageTypeUuid: pageType.uuid,
		});

		return this.pageRepository.update(page.uuid, {
			...updatedPage,
			uuid: page.uuid,
			updatedAt: new Date(),
		});
	}

	public async delete(pageTypeId: string, pageId: string): Promise<void> {
		await this.pageRepository.delete(pageId);
		return;
	}
}
