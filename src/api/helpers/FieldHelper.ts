import { Service } from 'typedi';
import { pathOr, prop, propOr } from 'ramda';

@Service()
export class FieldHelper {
	public mapTranslations(fields: any, contentType: any, language: string): any {
		// Look for fields that have multiLanguage set to true
		const multiLanguageFields = contentType.fields.reduce((acc, field) => {
			if (field.multiLanguage) {
				acc.push(field.slug);
			}

			return acc;
		}, []);

		const reducedFields = multiLanguageFields.reduce((acc, field) => {
			const defaultLanguageKey = Object.keys(propOr({}, field)(fields))[0];

			return {
				...acc,
				[field]: pathOr(pathOr(prop(field)(fields), [field, defaultLanguageKey])(fields), [field, language])(fields),
			};
		}, fields);

		return reducedFields;
	}
}
