import { Service } from 'typedi';
import { Machine } from 'xstate';

import { Workflow } from '../models/Workflow';
import { WorkflowState } from '../models/WorkflowState';
import { pick, pathOr, lensPath, set } from 'ramda';

@Service()
export class StateHelper {
	public getActions(workflow: Workflow, stateId?: string): any {
		const machine = Machine({
			id: workflow.slug,
			initial: stateId || workflow.initial,
			states: workflow.states.reduce((acc, state: WorkflowState) => {
				return {
					...acc,
					[state.slug]: pick(['on', 'meta'])(state),
				};
			}, {}),
		});

		return machine.initialState.nextEvents;
	}

	public transition(workflow: Workflow, currentState: string, action?: string): any {
		const machine = Machine({
			id: workflow.slug,
			initial: workflow.initial,
			states: workflow.states.reduce((acc, state: WorkflowState) => {
				return {
					...acc,
					[state.slug]: pick(['on', 'meta'])(state),
				};
			}, {}),
		});

		const nextStep = machine.transition(currentState, action);

		return {
			newState: nextStep.value,
			patches: pathOr([], [`${workflow.slug}.${nextStep.value}`, 'patches'])(nextStep.meta),
		};
	}

	public applyPatches(content: any, patches: any[]): any {
		return patches.reduce((acc, patch) => {
			const lens = lensPath([...patch.path.split('.')]);
			if (patch.value === 'true') {
				return set(lens, true)(acc);
			}

			if (patch.value === 'false') {
				return set(lens, false)(acc);
			}

			return set(lens, patch.value)(acc);
		}, content);
	}
}
