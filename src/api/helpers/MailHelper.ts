import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import nodemailer from 'nodemailer';
import { compile } from 'handlebars';
import mjml2html from 'mjml';
import fs from 'fs';
import path from 'path';
import { pathOr } from 'ramda';

import { TenantRepository } from '../repositories/TenantRepository';
import { env } from '../../env';

interface MailArguments {
	to: string;
	template: string;
	context: any;
	tenant?: string;
	subject: string;
}

@Service()
export class MailHelper {
	constructor(
		@OrmRepository() private tenantRepository: TenantRepository
	) {}

	public async sendMail({
		to, template, context, tenant: tenantUuid, subject,
	}: MailArguments): Promise<void> {
		const mailTemplate = compile(fs.readFileSync(path.join(process.cwd(), `src/mail/templates/${template}.mjml`)).toString());
		const html = mjml2html(mailTemplate({ ...context, env })).html;

		if (tenantUuid) {
			const tenant = await this.tenantRepository.findOne(tenantUuid);

			const tenantTransporter = nodemailer.createTransport({
				host: pathOr(undefined, ['mail', 'config', 'host'])(tenant),
				port: pathOr(undefined, ['mail', 'config', 'port'])(tenant),
				secure: pathOr(undefined, ['mail', 'config', 'secure'])(tenant),
				auth: {
				  user: pathOr(undefined, ['mail', 'config', 'user'])(tenant),
				  pass: pathOr(undefined, ['mail', 'config', 'password'])(tenant),
				},
			});

			return await tenantTransporter.sendMail({
				from: '"Inhoud Beheer Systeem" <mailer@ibs.sh>',
				to,
				subject,
				html,
			});
		}

		const transporter = nodemailer.createTransport({
			host: env.mail.host,
			port: env.mail.port,
			secure: env.mail.secure,
			auth: {
			  user: env.mail.user,
			  pass: env.mail.password,
			},
		});

		return await transporter.sendMail({
			from: '"Inhoud Beheer Systeem" <mailer@ibs.sh>',
			to,
			subject,
			html,
		});
	}
}
