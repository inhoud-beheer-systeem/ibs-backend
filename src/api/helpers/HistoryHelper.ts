import { Service } from 'typedi';
import { OrmRepository } from 'typeorm-typedi-extensions';
import { compare } from 'fast-json-patch';

import { Content } from '../models/Content';
import { ContentHistoryRepository } from '../repositories/ContentHistoryRepository';
import uuid = require('uuid');

@Service()
export class HistoryHelper {
	constructor(
		@OrmRepository() private contentHistoryRepository: ContentHistoryRepository
	) {}

	public async createHistoryItem(oldContent: Content, newContent: Content): Promise<Content> {
		const patches = compare(oldContent.fields, newContent.fields);

		await this.contentHistoryRepository.save({
			uuid: uuid.v4(),
			patches,
			tenantUuid: oldContent.tenantUuid,
			contentUuid: oldContent.uuid,
			createdAt: new Date(),
			updatedAt: new Date(),
		});

		return newContent;
	}
}
