import { Service } from 'typedi';
import { StorageServiceFactory } from '../../types/Storage';

type StorageType = 'ftp' | 's3bucket';

@Service()
export class StorageLoader {
	public load(storageType: StorageType): StorageServiceFactory {
		return require(`../../storage/${storageType}`).default;
	}
}
