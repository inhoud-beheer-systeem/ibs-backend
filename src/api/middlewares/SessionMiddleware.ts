import * as express from 'express';
import * as jwt from 'jsonwebtoken';
import { ExpressMiddlewareInterface, Middleware } from 'routing-controllers';
import { pathOr } from 'ramda';

@Middleware({ type: 'before' })
export class SessionMiddleware implements ExpressMiddlewareInterface {

	public use(req: express.Request, res: express.Response, next: express.NextFunction): any {
		const token = pathOr('', ['headers', 'authorization'])(req).replace('Bearer ', '');

		if (!token || token === 'null') {
			return next();
		}

		const bearerInfo = jwt.decode(token) as any;

		if (!bearerInfo) {
			return next();
		}

		res.locals.user = bearerInfo.user;

		return next();
	}

}
