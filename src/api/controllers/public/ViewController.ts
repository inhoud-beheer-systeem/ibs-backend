import {
	Get, JsonController, Param, HeaderParam, QueryParam
} from 'routing-controllers';

import { View } from '../../models/View';
import { ViewService } from '../../services/ViewService';
import { PaginatedContent } from '../../types/PaginatedContent';
import { PopulatorHelper } from '../../helpers/PopulatorHelper';
import { FieldHelper } from '../../helpers/FieldHelper';

@JsonController('/v1/views')
export class ViewController {
	constructor(
		private viewService: ViewService,
		private populatorHelper: PopulatorHelper,
		private fieldHelper: FieldHelper
	) { }

	@Get('/')
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<View[]>> {
		return this.viewService.find(tenant);
	}

	@Get('/:viewId')
	public async preview(
		@Param('viewId') viewId: string,
		@QueryParam('populate') populate: string,
		@QueryParam('language') language: string
	): Promise<any> {
		const view = await this.viewService.findOne(viewId);

		const content = await this.viewService.build({
			contentTypeUuid: view.contentTypeUuid,
			viewType: view.viewType,
			configuration: view.configuration,
		});

		if (!populate) {
			return {
				_entities: content.map((item) => ({
					...item,
					fields: this.fieldHelper.mapTranslations(item.fields, item.contentType, language),
				})),
				_page: {
					totalEntities: content.length,
				},
			};
		}

		const populatedContent = await this.populatorHelper.populateContent(content);

		return {
			_entities: populatedContent.map((item) => ({
				...item,
				fields: this.fieldHelper.mapTranslations(item.fields, item.contentType, language),
			})),
			_page: {
				totalEntities: populatedContent.length,
			},
		};
	}
}
