import { Get, HeaderParam, JsonController, Param, QueryParam } from 'routing-controllers';

import { Page } from '../../models/Page';
import { EntityHitService } from '../../services/EntityHitService';
import { PageService } from '../../services/PageService';
import { FieldHelper } from '../../helpers/FieldHelper';

@JsonController('/v1/pages')
export class PageController {

	constructor(
		private pageService: PageService,
		private entityHitService: EntityHitService,
		private fieldHelper: FieldHelper
	) { }

	@Get('/:pageTypeId')
	public async findOne(
		@Param('pageTypeId') pageTypeId: string,
		@HeaderParam('X-Tenant') tenant: string,
		@QueryParam('language') language: string
	): Promise<Page> {
		const page = await this.pageService.findOne(pageTypeId, {
			populate: true,
		});

		this.entityHitService.create('page', page.uuid, tenant);

		return {
			...page,
			fields: this.fieldHelper.mapTranslations(page.fields, page.pageType, language),
		};
	}
}
