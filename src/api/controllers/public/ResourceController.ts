import { Response } from 'express';
import { Controller, Get, QueryParams, Res } from 'routing-controllers';
import sharp from 'sharp';
import slugify from 'slugify';

import { ImageCacheService } from '../../services/ImageCacheService';
import { TenantService } from '../../services/TenantService';
import { StorageLoader } from '../../helpers/StorageLoader';

@Controller('/v1/resources')
export class ContentTypeController {
	constructor(
		private imageCacheService: ImageCacheService,
		private tenantService: TenantService,
		private storageLoader: StorageLoader
	) {}

	@Get()
	public async find(@QueryParams() params: any, @Res() response: Response): Promise<any> {
		response.setHeader('Cache-Control', 'max-age: 2419200');
		response.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());
		const cachePath = `${slugify(params.path)}-h_${params.h}-w_${params.w}-q_${params.q}-f_${params.f}`;

		const cachedImage = await this.imageCacheService.findOne(params.tenant, cachePath);

		if (!params.f || params.f === 'png') {
			response.setHeader('Content-Type', 'image/png');
		} else if (params.f === 'jpg' || params.f === 'jpeg') {
			response.setHeader('Content-Type', 'image/jpeg');
		}

		if (cachedImage) {
			response.setHeader('X-Cache', 'HIT');
			response.end(cachedImage.data, 'UTF-8');
			return response;
		}

		const tenant = await this.tenantService.findOne({ uuid: params.tenant });

		const StorageClient = this.storageLoader.load('ftp');
		const client = new StorageClient(tenant.storage.config);
		await client.init();

		const stream = await client.get(params.path.replace(/^\//, ''));

		const resizer = sharp()
			.resize({
				...(params.w && { width: parseInt(params.w, 10) }),
				...(params.h && { height: parseInt(params.h, 10) }),
				fit: 'cover',
			});

		if (!params.f || params.f === 'png') {
			resizer.png();
		} else if (params.f === 'jpg' || params.f === 'jpeg') {
			resizer.jpeg({
				...(params.q && { quality: parseInt(params.q, 10) }),
			});
		}

		const resizeStream = stream.pipe(resizer);
		const chunks = [];

		resizeStream.on('data', (chunk) => chunks.push(chunk));

		resizeStream.on('end', () => {
			const imageData = Buffer.concat(chunks);
			this.imageCacheService.create(params.tenant, imageData, cachePath);
		});

		response.setHeader('X-Cache', 'MISS');
		return resizeStream;
	}
}
