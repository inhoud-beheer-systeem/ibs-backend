import { Get, HeaderParam, JsonController, Param, QueryParam } from 'routing-controllers';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

import { Content } from '../../models/Content';
import { ContentService } from '../../services/ContentService';
import { EntityHitService } from '../../services/EntityHitService';
import { FieldHelper } from '../../helpers/FieldHelper';

@JsonController('/v1/content')
export class ContentController {

	constructor(
		private contentService: ContentService,
		private fieldHelper: FieldHelper,
		private entityHitService: EntityHitService
	) { }

	@Get('/:contentTypeId')
	public async findBContentType(
		@HeaderParam('X-Tenant') tenant: string,
		@Param('contentTypeId') contentTypeSlug: string,
		@QueryParam('filter') filters: any,
		@QueryParam('skip') skip: string = '0',
		@QueryParam('limit') limit: string = '20',
		@QueryParam('populate') populate: boolean = false,
		@QueryParam('sort') sort: any,
		@QueryParam('language') language: string
	): Promise<PaginatedContent<Content[]>> {
		return await this.contentService.findByContentType({
			tenant,
			contentTypeSlug,
			filters,
			skip: parseInt(skip, 10),
			limit: parseInt(limit, 10),
			sort,
			populate,
			language,
			showUnpublished: false,
		});
	}

	@Get('/:contentTypeId/:contentId')
	public async one(
		@Param('contentTypeId') contentTypeId: string,
		@Param('contentId') contentId: string,
		@HeaderParam('X-Tenant') tenant: string,
		@QueryParam('language') language: string
	): Promise<Content | undefined> {
		const content = await this.contentService.findOne(contentTypeId, contentId, {
			populate: true,
		});

		this.entityHitService.create('content', content.uuid, tenant);

		return {
			...content,
			fields: this.fieldHelper.mapTranslations(content.fields, content.contentType, language),
		};
	}
}
