import { Response } from 'express';
import { Controller, Get, QueryParams, Res } from 'routing-controllers';

import { TenantService } from '../../services/TenantService';
import { StorageLoader } from '../../helpers/StorageLoader';

@Controller('/v1/storage')
export class ContentTypeController {
	constructor(
		private tenantService: TenantService,
		private storageLoader: StorageLoader
	) {}

	@Get()
	public async find(@QueryParams() params: any, @Res() response: Response): Promise<any> {
		const StorageClient = this.storageLoader.load('ftp');
		response.setHeader('Content-Type', 'image/png');
		response.setHeader('Cache-Control', 'max-age: 2419200');
		response.setHeader('Expires', new Date(Date.now() + 2592000000).toUTCString());

		response.setHeader('X-Cache', 'MISS');

		const tenant = await this.tenantService.findOne({ uuid: params.tenant });
		const client = new StorageClient(tenant.storage.config);
		await client.init();
		const stream = await client.get(params.path.replace(/^\//, ''));
		return stream;
	}
}
