import { Authorized, Get, JsonController } from 'routing-controllers';

@JsonController('/permissions')
@Authorized()
export class ContentFieldController {
	@Get()
	public find(): any[] {
		return [
			{
				name: 'General',
				permissions: [
					{
						label: 'Dashboard',
						value: 'dashboard/read',
					},
					{
						label: 'Resources',
						value: 'resources/read',
					},
				],
			},
			{
				name: 'Views',
				permissions: [
					{
						label: 'Create views',
						value: 'views/create',
					},
					{
						label: 'Read views',
						value: 'views/read',
					},
					{
						label: 'Update views',
						value: 'views/update',
					},
					{
						label: 'Delete views',
						value: 'views/delete',
					},
				],
			},
			{
				name: 'Taxonomy',
				permissions: [
					{
						label: 'Create taxonomy',
						value: 'taxonomy/create',
					},
					{
						label: 'Read taxonomy',
						value: 'taxonomy/read',
					},
					{
						label: 'Update taxonomy',
						value: 'taxonomy/update',
					},
					{
						label: 'Delete taxonomy',
						value: 'taxonomy/delete',
					},
				],
			},
			{
				name: 'Pages',
				permissions: [
					{
						label: 'Create pages',
						value: 'pages/create',
					},
					{
						label: 'Read pages',
						value: 'pages/read',
					},
					{
						label: 'Update pages',
						value: 'pages/update',
					},
					{
						label: 'Delete pages',
						value: 'pages/delete',
					},
				],
			},
			{
				name: 'Content',
				permissions: [
					{
						label: 'Create content',
						value: 'content/create',
					},
					{
						label: 'Read content',
						value: 'content/read',
					},
					{
						label: 'Update content',
						value: 'content/update',
					},
					{
						label: 'Delete content',
						value: 'content/delete',
					},
				],
			},
			{
				name: 'Page types',
				permissions: [
					{
						label: 'Create page types',
						value: 'page-types/create',
					},
					{
						label: 'Read page types',
						value: 'page-types/read',
					},
					{
						label: 'Update page types',
						value: 'page-types/update',
					},
					{
						label: 'Delete page types',
						value: 'page-types/delete',
					},
				],
			},
			{
				name: 'Content types',
				permissions: [
					{
						label: 'Create content types',
						value: 'content-types/create',
					},
					{
						label: 'Read content types',
						value: 'content-types/read',
					},
					{
						label: 'Update content types',
						value: 'content-types/update',
					},
					{
						label: 'Delete content types',
						value: 'content-types/delete',
					},
				],
			},
			{
				name: 'Users',
				permissions: [
					{
						label: 'Create users',
						value: 'users/create',
					},
					{
						label: 'Read users',
						value: 'users/read',
					},
					{
						label: 'Update users',
						value: 'users/update',
					},
					{
						label: 'Delete users',
						value: 'users/delete',
					},
				],
			},
			{
				name: 'Roles',
				permissions: [
					{
						label: 'Create roles',
						value: 'roles/create',
					},
					{
						label: 'Read roles',
						value: 'roles/read',
					},
					{
						label: 'Update roles',
						value: 'roles/update',
					},
					{
						label: 'Delete roles',
						value: 'roles/delete',
					},
				],
			},
			{
				name: 'Languages',
				permissions: [
					{
						label: 'Create languages',
						value: 'languages/create',
					},
					{
						label: 'Read languages',
						value: 'languages/read',
					},
					{
						label: 'Update languages',
						value: 'languages/update',
					},
					{
						label: 'Delete languages',
						value: 'languages/delete',
					},
				],
			},
		];
	}
}
