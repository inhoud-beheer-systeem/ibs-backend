import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

import { TenantLanguage } from '../../models/TenantLanguage';
import { TenantLanguageService } from '../../services/TenantLanguageService';

import uuid = require('uuid');

@JsonController('/tenant-languages')
@Authorized()
export class TenantLanguageController {

	constructor(
		private tenantLanguageService: TenantLanguageService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<TenantLanguage[]>> {
		return this.tenantLanguageService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<TenantLanguage | undefined> {
		return this.tenantLanguageService.findOne(id);
	}

	@Post()
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		tenantLanguage.uuid = uuid.v4();
		tenantLanguage.createdAt = new Date();
		tenantLanguage.updatedAt = new Date();
		tenantLanguage.tenantUuid = tenant;
		return this.tenantLanguageService.create(tenantLanguage);
	}

	@Put('/:id')
	public async update(@Param('id') id: string, @Body() tenantLanguage: TenantLanguage): Promise<TenantLanguage> {
		tenantLanguage.updatedAt = new Date();
		return this.tenantLanguageService.update(id, tenantLanguage);
	}

	@Delete('/:id')
	public delete(@Param('id') id: string): Promise<void> {
		return this.tenantLanguageService.delete(id);
	}

}
