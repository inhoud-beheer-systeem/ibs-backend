import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';

import { Content } from '../../models/Content';
import { View } from '../../models/View';
import { ViewService } from '../../services/ViewService';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

@JsonController('/views')
@Authorized()
export class ViewController {
	constructor(
		private viewService: ViewService
	) { }

	@Get('/')
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<View[]>> {
		return this.viewService.find(tenant);
	}

	@Post('/preview')
	public preview(@Body() content: any): Promise<Content[] | undefined> {
		return this.viewService.build({
			contentTypeUuid: content.contentType,
			viewType: content.viewType,
			configuration: content.configuration,
		});
	}

	@Get('/:viewId')
	public one(@Param('viewId') viewId: string): Promise<View | undefined> {
		return this.viewService.findOne(viewId);
	}

	@Post('/')
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() content: View): Promise<View> {
		return this.viewService.create(tenant, content);
	}

	@Put('/:viewId')
	public update(@Param('viewId') viewId: string, @Body() content: View): Promise<View> {
		return this.viewService.update(viewId, content);
	}

	@Delete('/:viewId')
	public async delete(@Param('viewId') viewId: string): Promise<any> {
		await this.viewService.delete(viewId);
		return {};
	}

}
