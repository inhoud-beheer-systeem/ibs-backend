import { Get, JsonController, HeaderParam } from 'routing-controllers';
import { TenantLanguageService } from '../../services/TenantLanguageService';

@JsonController('/languages')
export class LanguageController {
	constructor(
		private tenantLanguageService: TenantLanguageService
	) {}

	@Get('/active')
	public findMe(@HeaderParam('X-Tenant') tenant: string): any {
		return this.tenantLanguageService.find(tenant);
	}

}
