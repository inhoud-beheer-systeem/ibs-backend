import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';

import { Taxonomy } from '../../models/Taxonomy';
import { TaxonomyService } from '../../services/TaxonomyService';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

@JsonController('/taxonomy')
@Authorized()
export class TaxonomyController {

	constructor(
		private taxonomyService: TaxonomyService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<Taxonomy[]>> {
		return this.taxonomyService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<Taxonomy | undefined> {
		return this.taxonomyService.findOne(id);
	}

	@Post()
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() taxonomy: Taxonomy): Promise<Taxonomy> {
		return this.taxonomyService.create(tenant, taxonomy);
	}

	@Put('/:id')
	public update(@HeaderParam('X-Tenant') tenant: string, @Param('id') id: string, @Body() taxonomy: Taxonomy): Promise<Taxonomy> {
		return this.taxonomyService.update(id, tenant, taxonomy);
	}

	@Delete('/:id')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.taxonomyService.delete(id);
		return {};
	}

}
