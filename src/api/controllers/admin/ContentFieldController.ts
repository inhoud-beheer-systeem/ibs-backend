import { Authorized, Get, JsonController } from 'routing-controllers';

@JsonController('/contentFields')
@Authorized()
export class ContentFieldController {
	@Get()
	public find(): any[] {
		return [
			{
				identifier: 'text-input',
				name: 'Text Field',
				description: 'This a useful text field',
				icon: 'bold',
				config: [
					{
						identifier: 'placeholder',
						name: 'Placeholder',
						component: 'text-input',
					},
				],
			},
			{
				identifier: 'textarea-input',
				name: 'Textarea',
				description: 'This is a text field but larger',
				icon: 'align-justify',
				config: [
					{
						identifier: 'placeholder',
						name: 'Placeholder',
						component: 'text-input',
					},
				],
			},
			{
				identifier: 'image-input',
				name: 'Image',
				description: 'Does what it sais',
				icon: 'image',
				config: [
					{
						identifier: 'multiple',
						name: 'Multiple',
						component: 'boolean-input',
					},
				],
			},
			{
				identifier: 'file-input',
				name: 'File',
				description: 'Does what it sais',
				icon: 'file',
				config: [
					{
						identifier: 'multiple',
						name: 'Multiple',
						component: 'boolean-input',
					},
					{
						identifier: 'allowedExtensions',
						name: 'Allowed file types (comma seperated)',
						component: 'text-input',
					},
				],
			},
			{
				identifier: 'content-input',
				name: 'Content',
				description: 'Add nested content',
				icon: 'code-branch',
				config: [
					{
						identifier: 'multiple',
						name: 'Multiple',
						component: 'boolean-input',
					},
					{
						identifier: 'contentType',
						name: 'Content type',
						component: 'contenttype-input',
					},
				],
			},
			{
				identifier: 'richtext-input',
				name: 'Rich text',
				description: 'Add some rich text',
				icon: 'text-fields',
				config: [
					{
						identifier: 'placeholder',
						name: 'Placeholder',
						component: 'text-input',
					},
				],
			},
			{
				identifier: 'boolean-input',
				name: 'Boolean',
				description: 'Add some boolean',
				icon: 'check-square',
				config: [],
			},
			{
				identifier: 'map-input',
				name: 'Map',
				description: 'Add some map',
				icon: 'map',
				config: [],
			},
			{
				identifier: 'repeater',
				name: 'Repeater',
				description: 'Repeat one (or more) fields',
				icon: 'repeat',
				config: [
					// {
					// 	identifier: 'multiple',
					// 	name: 'Multiple',
					// 	component: 'boolean-input',
					// },
				],
			},
			{
				identifier: 'taxonomy-input',
				name: 'Taxonomy',
				description: 'Use taxonomy',
				icon: 'border-alt',
				config: [
					{
						identifier: 'multiple',
						name: 'Multiple',
						component: 'boolean-input',
					},
					{
						identifier: 'taxonomy',
						name: 'Taxonomy',
						component: 'taxonomy-group-input',
					},
				],
			},
		];
	}
}
