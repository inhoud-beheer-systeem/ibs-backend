import * as bcryptjs from 'bcryptjs';
import {
	Authorized, Body, Delete, Get, JsonController, OnUndefined, Param, Post, Put
} from 'routing-controllers';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

import { UserNotFoundError } from '../../errors/UserNotFoundError';
import { User } from '../../models/User';
import { UserService } from '../../services/UserService';

import uuid = require('uuid');

@JsonController('/users')
@Authorized()
export class UserController {

	constructor(
		private userService: UserService
	) { }

	@Get()
	public find(): Promise<PaginatedContent<User[]>> {
		return this.userService.find();
	}

	@Get('/:id')
	@OnUndefined(UserNotFoundError)
	public one(@Param('id') id: string): Promise<User | undefined> {
		return this.userService.findOne({ uuid: id });
	}

	@Post()
	public create(@Body() user: User): Promise<User> {
		user.uuid = uuid.v4();
		user.createdAt = new Date();
		user.updatedAt = new Date();
		return this.userService.create(user);
	}

	@Put('/:id')
	public async update(@Param('id') id: string, @Body() user: User): Promise<User> {
		const existingUser = await this.userService.findOne(id);

		if (!user.password) {
			user.password = existingUser.password;
		} else {
			user.password = bcryptjs.hashSync(user.password);
		}
		user.updatedAt = new Date();

		return this.userService.update(id, user);
	}

	@Delete('/:id')
	public delete(@Param('id') id: string): Promise<void> {
		return this.userService.delete(id);
	}

}
