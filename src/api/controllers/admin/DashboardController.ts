import moment from 'moment';
import { Authorized, Get, HeaderParam, JsonController } from 'routing-controllers';

import { EntityHitService } from '../../services/EntityHitService';

@JsonController('/dashboard')
@Authorized()
export class UserController {

	constructor(
		private entityHitService: EntityHitService
	) { }

	@Get()
	public async dashboard(@HeaderParam('X-Tenant') tenant: string): Promise<any> {
		const popularContent = await this.entityHitService.getPopularEntities('content', tenant);
		const popularPages = await this.entityHitService.getPopularEntities('page', tenant);
		const hours = await this.entityHitService.getCountByDay(tenant);

		return {
			hits: {
				lastHour: await this.entityHitService.getCountFromDate(moment().subtract(1, 'hour').toDate(), tenant),
				lastDay: await this.entityHitService.getCountFromDate(moment().subtract(1, 'day').toDate(), tenant),
				lastWeek: await this.entityHitService.getCountFromDate(moment().subtract(1, 'week').toDate(), tenant),
				lastMonth: await this.entityHitService.getCountFromDate(moment().subtract(1, 'month').toDate(), tenant),
				lastYear: await this.entityHitService.getCountFromDate(moment().subtract(1, 'year').toDate(), tenant),
				allTime: await this.entityHitService.getCountFromDate(moment().subtract(10, 'years').toDate(), tenant),
			},
			hours: [
				{
					name: 'Views',
					series: hours,
				},
			],
			popularContent,
			popularPages,
		};
	}
}
