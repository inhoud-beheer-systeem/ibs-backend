import {
	Authorized, Body, Get, JsonController, Post, OnUndefined, HeaderParam, BadRequestError, Param, Delete, Res
} from 'routing-controllers';
import { classToPlain } from 'class-transformer';
import { PaginatedContent } from '../../types/PaginatedContent';
import * as bcryptjs from 'bcryptjs';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';

import { env } from '../../../env';
import { UserService } from '../../services/UserService';
import { InviteService } from '../../services/InviteService';
import { TenantService } from '../../services/TenantService';
import { Invite } from '../../models/Invite';
import { User } from '../../models/User';
import uuid = require('uuid');

@JsonController('/invites')
export class InviteController {

	constructor(
		private userService: UserService,
		private inviteService: InviteService,
		private tenantService: TenantService
	) { }

	@Post('/:inviteUuid/resend')
	@OnUndefined(204)
	@Authorized()
	public async resendInvite(@HeaderParam('X-Tenant') tenantUuid: string, @Param('inviteUuid') inviteUuid: string): Promise<void> {
		const invite = await this.inviteService.findOne(inviteUuid);
		const tenant = await this.tenantService.findOne(tenantUuid);
		const user = await this.userService.findOne({ email: invite.emailAddress });

		if (!user) {
			// No user exists. invite them to IBS
			this.inviteService.sendRegistrationInviteMail(tenant, invite);
		} else {
			// user exists, send out tenant request
			this.inviteService.sendTenantInviteMail(tenant, invite, user);
		}

		return;
	}

	@Post('/:inviteUuid/register')
	@OnUndefined(204)
	public async register(@Param('inviteUuid') inviteUuid: string, @Body() user: User): Promise<void> {
		const invite = await this.inviteService.findOne(inviteUuid);

		const createdUser = await this.userService.create({
			uuid: uuid.v4(),
			...user,
			password: bcryptjs.hashSync(user.password),
			email: invite.emailAddress,
			tenants: [invite.tenant.uuid],
			updatedAt: new Date(),
			createdAt: new Date(),
		} as any);

		await this.userService.assignRole(createdUser.uuid, invite.tenant.uuid, invite.role.uuid);
		await this.inviteService.delete(inviteUuid);

		return;
	}

	@Post('/:inviteUuid/accept')
	@OnUndefined(204)
	@Authorized()
	public async acceptInvite(@Param('inviteUuid') inviteUuid: string, @Res() res: express.Response): Promise<any> {
		const invite = await this.inviteService.findOne(inviteUuid);
		const existingUser = await this.userService.findOne(res.locals.user.uuid);

		// TODO: fix this disgusting shit ╏つ ͜ಠ ‸ ͜ಠ ╏つ, it is absolutely disgusting
		existingUser.tenants = [invite.tenantUuid, ...existingUser.tenants.map((x) => x.uuid)] as any;

		// push the new tenant
		await this.userService.update(existingUser.uuid, existingUser);
		await this.userService.assignRole(existingUser.uuid, invite.tenant.uuid, invite.role.uuid);
		await this.inviteService.delete(inviteUuid);

		const updatedUser = await this.userService.findOne(res.locals.user.uuid);

		return {
			token: jwt.sign(classToPlain({
				user: updatedUser,
				tenants: updatedUser.tenants,
			}), env.jwt.privateKey),
		};
	}

	@Get('/:inviteUuid')
	public findOne(@Param('inviteUuid') inviteUuid: string): Promise<Invite> {
		return this.inviteService.findOne(inviteUuid);
	}

	@Get()
	@Authorized()
	public find(@HeaderParam('X-Tenant') tenantUuid: string): Promise<PaginatedContent<Invite[]>> {
		return this.inviteService.find(tenantUuid);
	}

	@Delete('/:inviteUuid')
	@Authorized()
	@OnUndefined(204)
	public delete(@Param('inviteUuid') inviteUuid: string): Promise<void> {
		return this.inviteService.delete(inviteUuid);
	}

	@Post()
	@Authorized()
	public async create(@HeaderParam('X-Tenant') tenantUuid: string, @Body() inviteData: Invite): Promise<Invite> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });

		// Check if user exists with that email
		const user = await this.userService.findOne({ email: inviteData.emailAddress });

		// Check if an invite already exists, if yes, then just use that to send the mail
		const existingInvite = await this.inviteService.findOne({
			tenantUuid,
			emailAddress: inviteData.emailAddress,
		});

		if (existingInvite) {
			throw new BadRequestError('USER_ALREADY_INVITED');
		}

		// Create an invite
		const invite = await this.inviteService.create(tenantUuid, inviteData);

		if (!user) {
			// No user exists. invite them to IBS
			this.inviteService.sendRegistrationInviteMail(tenant, invite);
		} else {
			// user exists, send out tenant request
			this.inviteService.sendTenantInviteMail(tenant, invite, user);
		}

		return this.inviteService.findOne(invite.uuid);
	}
}
