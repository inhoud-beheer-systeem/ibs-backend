import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';

import { PageType } from '../../models/PageType';
import { PageTypeService } from '../../services/PageTypeService';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

@JsonController('/pageTypes')
@Authorized()
export class PageTypeController {

	constructor(
		private pageTypeService: PageTypeService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<PageType[]>> {
		return this.pageTypeService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<PageType | undefined> {
		return this.pageTypeService.findOne(id);
	}

	@Post()
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() contentType: PageType): Promise<PageType> {
		return this.pageTypeService.create(tenant, contentType);
	}

	@Put('/:id')
	public update(@HeaderParam('X-Tenant') tenant: string, @Param('id') id: string, @Body() contentType: PageType): Promise<PageType> {
		return this.pageTypeService.update(id, tenant, contentType);
	}

	@Delete('/:id')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.pageTypeService.delete(id);
		return {};
	}

}
