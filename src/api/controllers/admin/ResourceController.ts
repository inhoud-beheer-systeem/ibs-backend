import { Authorized, Get, JsonController, QueryParam, HeaderParam, Post, OnUndefined, Delete, UploadedFile } from 'routing-controllers';
import { TenantService } from '../../services/TenantService';
import { StorageLoader } from '../../helpers/StorageLoader';
import { StorageItem } from '../../../types/Storage';

@JsonController('/resources')
@Authorized()
export class ResourceController {
	constructor(
		private tenantService: TenantService,
		private storageLoader: StorageLoader
	) {}

	@Get()
	public async list(@QueryParam('dir') dir: string = '', @HeaderParam('X-Tenant') tenantUuid: string): Promise<StorageItem[]> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.list(dir);
	}

	@Post()
	@OnUndefined(204)
	public async upload(
		@QueryParam('dir') dir: string = '',
		@HeaderParam('X-Tenant') tenantUuid: string,
		@UploadedFile('file') file: Express.Multer.File
	): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.put(`${dir}/${file.originalname}`.replace(/^\//, ''), file.buffer);
	}

	@Delete()
	@OnUndefined(204)
	public async delete(
		@QueryParam('dir') dir: string = '',
		@HeaderParam('X-Tenant') tenantUuid: string
	): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		return await client.delete(dir.replace(/^\//, ''));
	}

	@Post('/directory')
	@OnUndefined(204)
	public async createDirectory(@QueryParam('dir') dir: string = '', @HeaderParam('X-Tenant') tenantUuid: string): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		await client.mkdir(dir);
		return;
	}

	@Delete('/directory')
	@OnUndefined(204)
	public async deleteDirectory(@QueryParam('dir') dir: string = '', @HeaderParam('X-Tenant') tenantUuid: string): Promise<void> {
		const tenant = await this.tenantService.findOne({ uuid: tenantUuid });
		const StorageService = this.storageLoader.load('ftp');
		const client = new StorageService(tenant.storage.config);
		await client.init();

		await client.rmdir(dir);
		return;
	}
}
