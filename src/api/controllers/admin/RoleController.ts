import {
	Authorized, Body, Delete, Get, HeaderParam, JsonController, Param, Post, Put
} from 'routing-controllers';

import { Role } from '../../models/Role';
import { RoleService } from '../../services/RoleService';
import { PaginatedContent } from '../../types/PaginatedContent';

import uuid = require('uuid');

@JsonController('/roles')
@Authorized()
export class RoleController {

	constructor(
		private roleService: RoleService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<Role[]>> {
		return this.roleService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<Role | undefined> {
		return this.roleService.findOne({ uuid: id });
	}

	@Post()
	public create(@Body() role: Role, @HeaderParam('X-Tenant') tenant: string): Promise<Role> {
		role.uuid = uuid.v4();
		role.createdAt = new Date();
		role.updatedAt = new Date();
		return this.roleService.create(role, tenant);
	}

	@Put('/:id')
	public async update(@Param('id') id: string, @Body() role: Role): Promise<Role> {
		return this.roleService.update(id, role);
	}

	@Delete('/:id')
	public delete(@Param('id') id: string): Promise<void> {
		return this.roleService.delete(id);
	}

}
