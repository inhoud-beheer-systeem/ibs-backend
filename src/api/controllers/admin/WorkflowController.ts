import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';

import { Workflow } from '../../models/Workflow';
import { WorkflowService } from '../../services/WorkflowService';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

@JsonController('/workflows')
@Authorized()
export class WorkflowController {

	constructor(
		private workflowService: WorkflowService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<Workflow[]>> {
		return this.workflowService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<Workflow | undefined> {
		return this.workflowService.findOne(id);
	}

	@Post()
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() workflow: Workflow): Promise<Workflow> {
		return this.workflowService.create(tenant, workflow);
	}

	@Put('/:id')
	public update(@HeaderParam('X-Tenant') tenant: string, @Param('id') id: string, @Body() workflow: Workflow): Promise<Workflow> {
		return this.workflowService.update(id, tenant, workflow);
	}

	@Delete('/:id')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.workflowService.delete(id);
		return {};
	}

}
