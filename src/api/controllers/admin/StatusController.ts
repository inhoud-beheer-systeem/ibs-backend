import { Get, JsonController, HeaderParam } from 'routing-controllers';
import path from 'path';

@JsonController('/status')
export class ViewController {
	@Get('/')
	public find(@HeaderParam('X-Tenant') tenant: string): any {
		const packageJson = require(path.join(process.cwd(), 'package.json'));
		return {
			status: 'ok',
			version: packageJson.version,
		};
	}
}
