import {
	Authorized, Body, Delete, Get, HeaderParam, JsonController, Param, Post, Put, QueryParam
} from 'routing-controllers';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

import { Content } from '../../models/Content';
import { ContentService } from '../../services/ContentService';

@JsonController('/content')
@Authorized()
export class ContentController {

	constructor(
		private contentService: ContentService
	) { }

	@Get('/')
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<Content[]> {
		return this.contentService.find(tenant);
	}

	@Get('/:contentTypeId')
	public findBContentType(
		@HeaderParam('X-Tenant') tenant: string,
		@Param('contentTypeId') contentTypeSlug: string,
		@QueryParam('filter') filters: any,
		@QueryParam('sort') sort: any
	): Promise<PaginatedContent<Content[]>> {
		return this.contentService.findByContentType({ tenant, contentTypeSlug, filters, sort, showUnpublished: true });
	}

	@Get('/:contentTypeId/:contentId')
	public one(@Param('contentTypeId') contentTypeId: string, @Param('contentId') contentId: string): Promise<Content | undefined> {
		return this.contentService.findOne(contentTypeId, contentId);
	}

	@Post('/:contentTypeId')
	public create(@HeaderParam('X-Tenant') tenant: string, @Param('contentTypeId') contentTypeId: string, @Body() content: Content): Promise<Content> {
		return this.contentService.create(tenant, contentTypeId, content);
	}

	@Put('/:contentTypeId/:contentId')
	public update(
		@Param('contentTypeId') contentTypeId: string,
		@Param('contentId') contentId: string,
		@Body() content: Content,
		@QueryParam('action') action: string
	): Promise<Content> {
		return this.contentService.update(contentTypeId, contentId, content, action);
	}

	@Delete('/:contentTypeId/:contentId')
	public async delete(@Param('contentTypeId') contentTypeId: string, @Param('contentId') contentId: string): Promise<any> {
		await this.contentService.delete(contentTypeId, contentId);
		return {};
	}

}
