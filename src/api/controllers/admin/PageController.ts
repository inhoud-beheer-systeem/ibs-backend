import {
	Authorized, Body, Delete, Get, JsonController, Param, Put, QueryParams
} from 'routing-controllers';

import { Page } from '../../models/Page';
import { PageService } from '../../services/PageService';

@JsonController('/pages')
@Authorized()
export class PageController {

	constructor(
		private pageService: PageService
	) { }

	@Get('/:pageTypeId')
	public findOne(@Param('pageTypeId') pageTypeId: string, @QueryParams() filters: any): Promise<Page> {
		return this.pageService.findOne(pageTypeId);
	}

	@Put('/:pageTypeId')
	public update(@Param('pageTypeId') pageTypeId: string, @Body() page: Page): Promise<Page> {
		return this.pageService.update(pageTypeId, page);
	}

	@Delete('/:pageTypeId')
	public async delete(@Param('pageTypeId') pageTypeId: string, @Param('pageId') pageId: string): Promise<any> {
		await this.pageService.delete(pageTypeId, pageId);
		return {};
	}

}
