import {
	Authorized, Body, Delete, Get, JsonController, Param, Post, Put, HeaderParam
} from 'routing-controllers';

import { ContentType } from '../../models/ContentType';
import { ContentTypeService } from '../../services/ContentTypeService';
import { PaginatedContent } from 'src/api/types/PaginatedContent';

@JsonController('/contentTypes')
@Authorized()
export class ContentTypeController {

	constructor(
		private contentTypeService: ContentTypeService
	) { }

	@Get()
	public find(@HeaderParam('X-Tenant') tenant: string): Promise<PaginatedContent<ContentType[]>> {
		return this.contentTypeService.find(tenant);
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<ContentType | undefined> {
		return this.contentTypeService.findOne(id);
	}

	@Post()
	public create(@HeaderParam('X-Tenant') tenant: string, @Body() contentType: ContentType): Promise<ContentType> {
		return this.contentTypeService.create(tenant, contentType);
	}

	@Put('/:id')
	public update(@HeaderParam('X-Tenant') tenant: string, @Param('id') id: string, @Body() contentType: ContentType): Promise<ContentType> {
		return this.contentTypeService.update(id, tenant, contentType);
	}

	@Delete('/:id')
	public async delete(@Param('id') id: string): Promise<any> {
		await this.contentTypeService.delete(id);
		return {};
	}

}
