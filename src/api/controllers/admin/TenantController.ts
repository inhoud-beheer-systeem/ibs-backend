import {
	Authorized, Body, Delete, Get, JsonController, OnUndefined, Param, Post, Put
} from 'routing-controllers';

import { Tenant } from '../../models/Tenant';
import { User } from '../../models/User';
import { TenantService } from '../../services/TenantService';
import { UserService } from '../../services/UserService';
import { PaginatedContent } from '../../types/PaginatedContent';

import uuid = require('uuid');
@JsonController('/tenants')
@Authorized()
export class TenantController {

	constructor(
		private tenantService: TenantService,
		private userService: UserService
	) { }

	@Get()
	public find(): Promise<PaginatedContent<Tenant[]>> {
		return this.tenantService.find();
	}

	@Get('/:id')
	public one(@Param('id') id: string): Promise<Tenant | undefined> {
		return this.tenantService.findOne({ uuid: id });
	}

	@Post()
	public create(@Body() tenant: Tenant): Promise<Tenant> {
		tenant.uuid = uuid.v4();
		tenant.createdAt = new Date();
		tenant.updatedAt = new Date();
		return this.tenantService.create(tenant);
	}

	@Put('/:id')
	public async update(@Param('id') id: string, @Body() tenant: Tenant): Promise<Tenant> {
		return this.tenantService.update(id, tenant);
	}

	@Delete('/:id')
	@OnUndefined(204)
	public delete(@Param('id') id: string): Promise<void> {
		return this.tenantService.delete(id);
	}

	@Get('/:id/users')
	public async findUsers(@Param('id') id: string): Promise<PaginatedContent<User[]> | undefined> {
		const tenant = await this.tenantService.findOne({ uuid: id });

		return {
			_entities: tenant.users,
			_page: {
				totalEntities: tenant.users.length,
			},
		};
	}

	@Get('/:id/users/:userUuid')
	public async findOneUser(@Param('id') tenantUuid: string, @Param('userUuid') userUuid: string): Promise<any> {
		const user = await this.userService.findOne({ uuid: userUuid });

		return {
			...user,
			role: await this.userService.getRole(userUuid, tenantUuid),
		};
	}

	@Put('/:id/users/:userUuid')
	public async updateUser(@Param('id') tenantUuid: string, @Param('userUuid') userUuid: string, @Body() user: any): Promise<User> {
		await this.userService.assignRole(userUuid, tenantUuid, user.role);
		return this.userService.findOne({ uuid: userUuid });
	}
}
