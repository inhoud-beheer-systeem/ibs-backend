import { classToPlain } from 'class-transformer';
import { Request, Response } from 'express';
import * as bcryptjs from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { Body, Get, JsonController, Post, Req, Res, UnauthorizedError, HeaderParam, OnUndefined, Param, Put } from 'routing-controllers';
import { pathOr, propOr } from 'ramda';

import { env } from '../../../env';
import { UserNotFoundError } from '../../errors/UserNotFoundError';
import { User } from '../../models/User';
import { UserService } from '../../services/UserService';
import { PasswordResetService } from '../../services/PasswordResetService';
import { PasswordReset } from '../../models/PasswordReset';

@JsonController('/auth')
export class UserController {
	constructor(
		private userService: UserService,
		private passwordResetService: PasswordResetService
	) { }

	@Post('/login')
	public async login(@Req() req: Request, @Body() body: any, @Res() res: Response): Promise<any> {
		const user = await this.userService.findOne({ email: body.email });

		if (!user) {
			throw new UserNotFoundError();
		}

		const isValidUser = await User.comparePassword(user, body.password);

		if (!isValidUser) {
			return res.sendStatus(401);
		}

		return {
			token: jwt.sign(classToPlain({
				user,
				tenants: user.tenants,
			}), env.jwt.privateKey),
		};
	}

	@Put('/password-reset/:passwordResetUuid')
	@OnUndefined(204)
	public async resetPassword(@Param('passwordResetUuid') passwordResetUuid: string, @Body() body: any): Promise<void> {
		const passwordReset = await this.passwordResetService.findOne(passwordResetUuid);
		const user = await this.userService.findOne({ email: passwordReset.emailAddress });

		user.password = bcryptjs.hashSync(body.password);
		await this.userService.update(user.uuid, user);

		await this.passwordResetService.delete(passwordResetUuid);

		return;
	}

	@Get('/password-reset/:passwordResetUuid')
	public async getPasswordReset(@Param('passwordResetUuid') passwordResetUuid: string): Promise<PasswordReset> {
		return await this.passwordResetService.findOne(passwordResetUuid);
	}

	@Post('/password-reset')
	@OnUndefined(204)
	public async createPasswordReset(@Body() body: any): Promise<void> {
		const user = await this.userService.findOne({ email: body.emailAddress });

		if (!user) {
			return;
		}

		const passwordReset = await this.passwordResetService.create(body);
		await this.passwordResetService.sendPasswordResetMail(passwordReset, user);

		return;
	}

	@Get('/check')
	public findMe(@Req() req: Request): Promise<User[]> {
		if (!req.session.user) {
			throw new UnauthorizedError();
		}

		return req.session.user;
	}

	@Get('/user')
	public async user(@Req() req: Request, @HeaderParam('X-Tenant') tenantUuid: string): Promise<any> {
		const token = pathOr('', ['headers', 'authorization'])(req).replace('Bearer ', '');

		const user = await this.userService.findOne({ uuid: (jwt.decode(token) as any).user.uuid });
		const role = await this.userService.getRole(user.uuid, tenantUuid);

		return {
			user,
			role,
			permissions: (propOr([], 'permissions')(role) as any[]).map((permission) => permission.permission),
		};
	}

}
