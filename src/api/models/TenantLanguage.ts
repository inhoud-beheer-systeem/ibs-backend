import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TenantLanguage {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public key: string;

	@Column()
	public label: string;

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
