import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { ContentType } from './ContentType';

@Entity()
export class ContentTypeField {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public order: number;

	@Column()
	public fieldType: string;

	@ManyToOne(() => ContentType, contentType => contentType.fields, {
		onDelete: 'CASCADE',
	})
	public contentType: ContentType;

	@Column()
	public contentTypeUuid: string;

	@OneToMany(() => ContentTypeField, contentTypeField => contentTypeField.parent, {
		cascade: true,
	})
	public subfields: ContentTypeField[];

	@ManyToOne(() => ContentTypeField, contentTypeField => contentTypeField.parentUuid)
	public parent: ContentTypeField;

	@Column()
	public parentUuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public config: any;

	@Column()
	public tenantUuid: string;

	@Column()
	public showOnOverview: boolean;

	@Column()
	public multiLanguage: boolean;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
