import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Content } from './Content';

@Entity()
export class ContentHistory {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public contentUuid: string;

	@Column()
	public tenantUuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public patches: any;

	@ManyToOne(() => Content, content => content.uuid)
	public content: Content;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
