import { Column, Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Tenant } from './Tenant';
import { Role } from './Role';

@Entity()
export class Invite {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public emailAddress: string;

	@Column()
	public tenantUuid: string;

	@ManyToOne(() => Tenant, {
		eager: true,
	})
	public tenant: Tenant;

	@Column()
	public roleUuid: string;

	@ManyToOne(() => Role, {
		eager: true,
	})
	public role: Role;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
