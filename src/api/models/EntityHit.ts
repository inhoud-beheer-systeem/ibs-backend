import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Content } from './Content';
import { Page } from './Page';

@Entity()
export class EntityHit {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public entityUuid: string;

	@Column()
	public entityType: string;

	@OneToOne(() => Content)
	@JoinColumn({
		name: 'entityUuid',
	})
	public content: Content;

	@OneToOne(() => Page)
	@JoinColumn({
		name: 'entityUuid',
	})
	public page: Page;

	@Column({ type: 'jsonb', nullable: true })
	public extraInfo: any;

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
