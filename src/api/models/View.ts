import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ContentType } from './ContentType';

@Entity()
export class View {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public configuration: any;

	@Column()
	public name: string;

	@Column()
	public description: string;

	@Column()
	public viewType: string;

	@Column()
	public contentTypeUuid: string;

	@ManyToOne(() => ContentType, contentType => contentType.uuid, {
		eager: true,
	})
	public contentType: ContentType;

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
