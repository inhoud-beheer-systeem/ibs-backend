import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

import { PageType } from './PageType';

@Entity()
export class PageTypeField {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public order: number;

	@Column()
	public fieldType: string;

	@Column()
	public pageTypeUuid: string;

	@ManyToOne(() => PageType, pageType => pageType.fields, {
		onDelete: 'CASCADE',
	})
	public pageType: PageType;

	@OneToMany(() => PageTypeField, pageTypeField => pageTypeField.parent, {
		cascade: true,
	})
	public subfields: PageTypeField[];

	@ManyToOne(() => PageTypeField, pageTypeField => pageTypeField.parentUuid)
	public parent: PageTypeField;

	@Column()
	public parentUuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public config: any;

	@Column()
	public tenantUuid: string;

	@Column()
	public multiLanguage: boolean;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
