import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Taxonomy } from './Taxonomy';

@Entity()
export class TaxonomyItem {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public label: string;

	@Column()
	public value: string;

	@Column()
	public tenantUuid: string;

	@Column()
	public taxonomyUuid: string;

	@ManyToOne(() => Taxonomy, taxonomy => taxonomy.items, {
		onDelete: 'CASCADE',
	})
	public taxonomy: Taxonomy;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
