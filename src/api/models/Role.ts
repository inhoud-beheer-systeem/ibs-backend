import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { RolePermission } from './RolePermission';

@Entity()
export class Role {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public name: string;

	@OneToMany(() => RolePermission, rolePermission => rolePermission.role, {
		eager: true,
		cascade: true,
		onDelete: 'CASCADE',
	})
	public permissions: RolePermission[];

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
