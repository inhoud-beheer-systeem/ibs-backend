import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Workflow } from './Workflow';

@Entity()
export class WorkflowState {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public workflowUuid: string;

	@ManyToOne(() => Workflow, workflow => workflow.states, {
		onDelete: 'CASCADE',
	})
	public workflow: Workflow;

	@Column({ type: 'jsonb', nullable: true })
	public on: any;

	@Column({ type: 'jsonb', nullable: true })
	public meta: any;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
