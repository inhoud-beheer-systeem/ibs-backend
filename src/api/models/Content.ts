import { IsNotEmpty } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ContentType } from './ContentType';

@Entity()
export class Content {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public fields: any;

	@Column()
	public contentTypeUuid: string;

	@Column()
	public tenantUuid: string;

	@IsNotEmpty()
	@Column()
	public name: string;

	@IsNotEmpty()
	@Column()
	public slug: string;

	@ManyToOne(() => ContentType, contentType => contentType.uuid, {
		eager: true,
	})
	public contentType: ContentType;

	@Column()
	public state: string;

	@Column()
	public published: boolean;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
