import { IsNotEmpty } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { PageTypeField } from './PageTypeField';

@Entity()
export class PageType {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@IsNotEmpty()
	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public description: string;

	@Column()
	public icon: string;

	@OneToMany(() => PageTypeField, pageTypeField => pageTypeField.pageType, {
		eager: true,
		cascade: true,
		onDelete: 'CASCADE',
	})
	public fields: PageTypeField[];

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
