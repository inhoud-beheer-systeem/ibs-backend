import { IsNotEmpty } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { WorkflowState } from './WorkflowState';

@Entity()
export class Workflow {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@IsNotEmpty()
	@Column()
	public name: string;

	@Column()
	public slug: string;

	@Column()
	public description: string;

	@OneToMany(() => WorkflowState, workflowState => workflowState.workflow, {
		eager: true,
		cascade: true,
		onDelete: 'CASCADE',
	})
	public states: WorkflowState[];

	@Column()
	public tenantUuid: string;

	@Column()
	public initial: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
