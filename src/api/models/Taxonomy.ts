import { IsNotEmpty } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { TaxonomyItem } from './TaxonomyItem';

@Entity()
export class Taxonomy {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@IsNotEmpty()
	@Column()
	public name: string;

	@IsNotEmpty()
	@Column()
	public slug: string;

	@Column()
	public description: string;

	@OneToMany(() => TaxonomyItem, taxonomyItem => taxonomyItem.taxonomy, {
		eager: true,
		cascade: true,
		onDelete: 'CASCADE',
	})
	public items: TaxonomyItem[];

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
