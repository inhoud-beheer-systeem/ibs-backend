import * as bcryptjs from 'bcryptjs';
import { Exclude } from 'class-transformer';
import { IsNotEmpty } from 'class-validator';
import {
	BeforeInsert, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn
} from 'typeorm';

import { Tenant } from './Tenant';

@Entity()
export class User {

	public static hashPassword(password: string): Promise<string> {
		return new Promise((resolve, reject) => {
			bcryptjs.hash(password, 10, (err, hash) => {
				if (err) {
					return reject(err);
				}
				resolve(hash);
			});
		});
	}

	public static comparePassword(user: User, password: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			bcryptjs.compare(password, user.password, (err, res) => {
				resolve(res === true);
			});
		});
	}

	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@IsNotEmpty()
	@Column()
	public firstName: string;

	@IsNotEmpty()
	@Column()
	public lastName: string;

	@IsNotEmpty()
	@Column()
	public email: string;

	@IsNotEmpty()
	@Exclude({
		toPlainOnly: true,
	})
	@Column()
	public password: string;

	@ManyToMany(type => Tenant, tenant => tenant.users, {
		cascade: true,
		eager: true,
	})
	@JoinTable()
	public tenants: Tenant[];

	@Column()
	public updatedAt: Date;

	public role?: any;

	@Column()
	public createdAt: Date;

	@BeforeInsert()
	public async hashPassword(): Promise<void> {
		this.password = await User.hashPassword(this.password);
	}

}
