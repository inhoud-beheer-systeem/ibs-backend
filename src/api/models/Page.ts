import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { PageType } from './PageType';

@Entity()
export class Page {
	@PrimaryGeneratedColumn('uuid')
	public uuid: string;

	@Column({ type: 'jsonb', nullable: true })
	public fields: any;

	@Column()
	public pageTypeUuid: string;

	@ManyToOne(() => PageType, pageType => pageType.uuid, {
		eager: true,
	})
	public pageType: PageType;

	@Column()
	public tenantUuid: string;

	@Column()
	public updatedAt: Date;

	@Column()
	public createdAt: Date;
}
