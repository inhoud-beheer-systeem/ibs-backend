import * as jwt from 'jsonwebtoken';
import { pathOr } from 'ramda';
import { Action } from 'routing-controllers';
import { Connection } from 'typeorm';

import { env } from '../env';

export function authorizationChecker(connection: Connection): (action: Action, roles: any[]) => Promise<boolean> | boolean {
	return async function innerAuthorizationChecker(action: Action, roles: string[]): Promise<boolean> {
		const token = pathOr('', ['headers', 'authorization'])(action.request).replace('Bearer ', '');

		if (!token) {
			return false;
		}

		try {
			return !!jwt.verify(token, env.jwt.privateKey);
		} catch (e) {
			return false;
		}
	};
}
