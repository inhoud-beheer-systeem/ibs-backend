export interface StorageItem {
	type: 'file' | 'directory';
	name: string;
	size: number;
	lastModified: Date;
	extension?: string;
	mimeType?: string;
}

export interface StorageService {
	init(): Promise<void>;
	get(path: string): Promise<NodeJS.ReadableStream>;
	list(path: string): Promise<StorageItem[]>;
	put(path: string, input: Buffer | NodeJS.ReadableStream): Promise<void>;
	delete(path: string): Promise<void>;
	mkdir(path: string): Promise<void>;
	rmdir(path: string): Promise<void>;
}

export type StorageServiceFactory = new (config: any) => StorageService;
